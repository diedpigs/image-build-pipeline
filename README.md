# Image Build Pipeline for Jetstream(2)

## Which problems does this solve?

When launching instances (a.k.a. servers, virtual machines) on a cloud computing service, one must use a [system disk image](https://en.wikipedia.org/wiki/Disk_image). This image contains the entire starting state of that instance's non-volatile memory (a.k.a. storage, root disk). This includes the operating system environment that it will boot from and a base set of software packages. Jetstream, like other cloud providers, must build and maintain a set of images for various supported instance starting configurations (different distros, NVIDIA drivers, etc.). These are typically called "base" or "featured" images.

It is no small effort to build, update, and maintain these images. Why? First, Jetstream customizes images to improve the performance and convenience of using the cloud. (For example, they have the Docker engine pre-installed, and some have NVIDIA drivers installed.) Sometimes, these customizations must be changed as the infrastructure and user needs evolve. Second, these images must also be updated frequently to incorporate operating system software updates that fix bugs and security problems.  When fixes for high-impact security issues become available in upstream operating systems, there is a need to update images quickly.

For the history of the Jetstream1 project, the process of updating images has been a mostly-manual one. There was a [partially-automated image update process](https://gitlab.com/jetstream-cloud/openstack-image-update-script), but it did not achieve sustained adoption. The manual process is, in very short: launch an instance from each current featured image, run operating system software updates, make any other needed customizations, shut those instances down, clean their disk images (delete log files, etc.), create new image from these instances, test the new images by creating more instance from them, and if everything looks right, publicize the new images, and deprecate the older images that you are replacing.

Being a manual effort, this update process was tedious and time-consuming, so it did not happen as often as it should. Featured images would go several months without updates, which means that instances launched from those older images would take a longer time to launch and become ready to use (because they had to install a lot of operating system updates), else remain unpatched for known security vulnerabilities. This manual update process also produced multi-generational images -- a copy of a copy of a copy, ad infinitum. This is bad because cruft data and files can accumulate in the image over time, especially if the cleaning process is not perfect. It becomes difficult to see the entire history and provenance of changes made since the image was last imported from the "upstream" operating system maintainer.

---

This project introduces a fully automated image build pipeline. It attempts to address the above deficiencies and pain points. Since it aims to remove the need for human intervention, it can be run very frequently -- weekly or even daily, ensuring that base/featured images are always current with software updates. 

## What does it do?

This project achieves a fully automated pipeline to build up-to-date base/featured images for Jetstream1/2. It uses [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) for continuous integration/deployment, and [Ansible](https://docs.ansible.com/ansible/latest/index.html) for image customization.

Every time this pipeline runs, it downloads the most recent versions of upstream images from the distro maintainer, and applies customizations to those images in a hopefully-declarative way. This pipeline does not re-use images or other image-like artifacts that it has previously customized, so we avoid making copies of copies ad infinitum, and we are not manually introducing one-off changes that would not be captured in the automation code. This should make any issues easier to reproduce and troubleshoot.

The CI pipeline currently runs every time a new commit is pushed to the upstream repository, or when it is invoked manually on the [GitLab project site](https://gitlab.com/jetstream-cloud/image-build-pipeline/-/pipelines). In the near future it will likely be configured to run on a schedule, or when new upstream images are published by distro maintainers.

## Tour of the code

(Editorial note: the following tour is correct at the time of writing, but may become out of date as the code evolves over time. Remember, comments and docs sometimes lie, but code never lies!)

### `.gitlab-ci.yml`

First, take a look at [.gitlab-ci.yml](.gitlab-ci.yml). This is the standard name for the [YAML](https://en.wikipedia.org/wiki/YAML) file where we define instructions for GitLab CI.  Here you'll see a single stage and job definition, `distro_specific_pipeline`.

This project uses [child pipelines](https://docs.gitlab.com/ee/ci/pipelines/parent_child_pipelines.html) to achieve independence of the build process for each image variant. Why is this needed? We want to build several different images for several operating systems, and if the image for one operating system fails to complete its build, we don't want that to cause all the other images to fail to build. We also don't want the CI jobs for each different image to run in lock-step, where all image variants must all complete one stage of the build process before moving on to the next stage. Child pipelines are the way to achieve this in GitLab CI. The [`trigger:`](https://docs.gitlab.com/ee/ci/yaml/index.html#trigger) keyword means that the child pipeline defined in `distro-specific-pipeline.yml` will run for each image variant -- we'll look there in a minute.

You'll see a [`parallel:`](https://docs.gitlab.com/ee/ci/yaml/index.html#parallel) keyword, which means that we are running multiple child pipelines at the same time. The parameters for these child pipelines are defined in the [`matrix:`](https://docs.gitlab.com/ee/ci/yaml/index.html#parallel-matrix-jobs) list. Each YAML map in that list represents an image to be built. Here we define some variables, like where to download the image from, and what to call the final result.

### `distro-specific-pipeline.yml`


Next, see [distro-specific-pipeline.yml](distro-specific-pipeline.yml). This defines the stages/jobs that occur for each image variant. Let's take it one stage at a time.


#### `get_base_image` stage

This stage downloads the operating system image from the upstream maintainer site, modifies its virtual size, and uploads it to OpenStack so that it can be customized.

You'll see that both Ubuntu and CentOS images come in [QCOW2 format](https://en.wikipedia.org/wiki/Qcow). You might say "Wait! Jetstream needs images to be in raw format, [for optimal usability and performance with the Ceph storage back-end](https://docs.ceph.com/en/latest/rbd/qemu-rbd/#creating-images-with-qemu). What's going on?" Don't worry, the raw format conversion happens later in the pipeline. The image is kept in QCOW2 format for now, because sparse QCOW2 images occupy much less space, which makes this stage run several minutes faster and consume many fewer GB of network transfer.

The one change that we make is setting the virtual size of the image. CentOS images have a 10 GB virtual size, and we shrink this to 8 GB, so that it can be used with the smallest flavor in OpenStack (m1.tiny, with an 8 GB root disk).

#### `create_build_instance` stage

Here, we launch an instance from the image that we just uploaded, so that we can install software and further customize the image.

#### `instance_setup` stage

Here is where the customization occurs. The code that does the customizing is defined as Ansible, so our goal here is just to run that code against the instance. Let's jump over to Ansible land. 

### Ansible `setup.yml`

Take a look at [`ansible/setup.yml`](ansible/setup.yml). First, this code waits for SSH connectivity -- the instance may still be booting up, so this will keep trying to connect until it works.

Then, we run setup/customization tasks. These are defined as discrete Ansible roles. Among other things, these roles install software packages, ensure they are up to date, and remove any unneeded packages.

Finally, we run `fstrim` to discard unused blocks of storage, and shut down the instance.

### `distro-specific-pipeline.yml`, continued

#### `snapshot_build_instance` stage

This stage takes an image of the instance that we just customized. First, it waits to ensure the instance is actually shut down -- we cannot make an image of it while it's powering off. Then, we create the image and wait until it reaches active status.

#### `clean_final_image` stage

We have just customized an instance and taken an image from it, but this image must be cleaned -- it will have log files, SSH authorized keys, and other "cruft" that we do not want in the final, master image.  This stage downloads the customized image and cleans it using [virt-sysprep](https://libguestfs.org/virt-sysprep.1.html). virt-sysprep does a pretty thorough job of cleaning log files and any junk out of user accounts.

Finally, this stage uploads the cleaned image to Glance again. This image is now considered the "candidate" master image, pending successful completion of the testing phase, which is next.

#### `create_test_image` stage

This stage creates an instance from the candidate image, so that we can make sure it works. It looks a lot like the `create_build_instance` stage, and in the future, maybe we will factor out the code so it can be re-used in both places. For now, it is just defined separately.

#### `instance_test` stage

This stage runs some Ansible code to test the instance we just created, to make sure that it works. Take a look at [`ansible/test.yml`](ansible/test.yml). Right now, all it does is ensure that we can create an SSH connection to the instance. In the future we should probably do a more thorough testing job.

#### `publish_image` stage

This stage runs only if the previous testing stage passed. Here, we will make the candidate image public, set the appropriate name, and deactivate the previous featured image that our new image replaces.

#### `delete_temp_resources` stage

We are done building, cleaning, testing, and publishing a new image. But, along the way we made a lot of stuff -- some instances with floating IPs, and a few temporary images in Glance. This code deletes all of those things that we don't need anymore. Note the `when: always`, which means that unlike all the other stages,  this stage always runs even if one of the previous stages fails.

---

The child pipeline for this image variant is now done! Note that the end status of the parent pipeline stage, `distro_specific_pipeline`, will be successful only if there are no failures in any of the child pipelines. If one of the image variant child pipelines fails to complete, the parent pipeline status will show failed, even if all of the pipelines for the other image variants finished without error.